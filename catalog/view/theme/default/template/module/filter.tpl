<!-- <div class="panel panel-default">
<h1>test_FILTER</h1>
  <div class="panel-heading"><?php echo $heading_title; ?></div>
  <div class="list-group">
    <?php foreach ($filter_groups as $filter_group) { ?>
    <a class="list-group-item"><?php echo $filter_group['name']; ?></a>
    <div class="list-group-item">
      <div id="filter-group<?php echo $filter_group['filter_group_id']; ?>">
        <?php foreach ($filter_group['filter'] as $filter) { ?>
        <div class="checkbox">
          <label>
            <?php if (in_array($filter['filter_id'], $filter_category)) { ?>
            <input type="checkbox" name="filter[]" value="<?php echo $filter['filter_id']; ?>" checked="checked" />
            <?php echo $filter['name']; ?>
            <?php } else { ?>
            <input type="checkbox" name="filter[]" value="<?php echo $filter['filter_id']; ?>" />
            <?php echo $filter['name']; ?>
            <?php } ?>
          </label>
        </div>
        <?php } ?>
      </div>
    </div>
    <?php } ?>
  </div>
  <div class="panel-footer text-right">
    <button type="button" id="button-filter" class="btn btn-primary"><?php echo $button_filter; ?></button>
  </div>
</div> -->

<div class="panel panel-default">
<h1>FILTER_NEW</h1>
<? //echo '<pre>'; var_dump($filter_group);die();?>
  <div class="panel-heading"><?php echo $heading_title; ?></div>
  <div class="list-group form-group">
    <?php foreach ($filter_groups as $filter_group) { ?>

    <!-- <label for="filter-group<?php echo $filter_group['filter_group_id']; ?>"><?php echo $filter_group['name']; ?></label> -->
    <select class="form-control" id="filter-group<?php echo $filter_group['filter_group_id']; ?>">
    <option data-parent="none"><?php echo $filter_group['name']; ?></option>
      <?php foreach ($filter_group['filter'] as $filter) { ?>
            <?php if (in_array($filter['filter_id'], $filter_category)) { ?>
            <option selected name="filter[]" data-parent="<?=$filter['filter_parent']?>" value="<?php echo $filter['filter_id']; ?>"><?php echo $filter['name']; ?></option>
            <?php echo $filter['name']; ?>
            <?php } else { ?>
            <option name="filter[]" data-parent="<?=$filter['filter_parent']?>" value="<?php echo $filter['filter_id']; ?>"><?php echo $filter['name']; ?></option>
            <?php echo $filter['name']; ?>
            <?php } ?>
      <?php } ?>
    </select>
    
    <?php } ?>
  </div>
  <div class="panel-footer text-right">
    <button type="button" id="button-filter" class="btn btn-primary"><?php echo $button_filter; ?></button>
  </div>
</div>


<script type="text/javascript"><!--

var magic_work = function() {

}

$(document).on('change', '#filter-group3', function() {
  var selected_item = $(this).find(':selected');
  var mark = selected_item.text();
  var selected_parent = selected_item.data('parent');
  if (selected_parent == 'none') {
    //block
    $('#filter-group4').prop('disabled', true);
  }
  else {
    //unblock
    $('#filter-group4').prop('disabled', false);

    $.each($('#filter-group4 option'), function(index, option) {
      var parent = $(this).data('parent');
      
      if (parent == mark) {
        $(this).show();
      }
      else {
        $(this).hide();
      }
    });
  }
});

$('#button-filter').on('click', function() {
	filter = [];

	$('input[name^=\'filter\']:checked').each(function(element) {
		filter.push(this.value);
	});

	location = '<?php echo $action; ?>&filter=' + filter.join(',');
});
//--></script>
