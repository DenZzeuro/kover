<?php
// HTTP
define('HTTP_SERVER', 'http://upload:8080/');

// HTTPS
define('HTTPS_SERVER', 'http://upload:8080/');

// DIR
define('DIR_APPLICATION', 'C:/OpenServer/domains/upload/catalog/');
define('DIR_SYSTEM', 'C:/OpenServer/domains/upload/system/');
define('DIR_IMAGE', 'C:/OpenServer/domains/upload/image/');
define('DIR_LANGUAGE', 'C:/OpenServer/domains/upload/catalog/language/');
define('DIR_TEMPLATE', 'C:/OpenServer/domains/upload/catalog/view/theme/');
define('DIR_CONFIG', 'C:/OpenServer/domains/upload/system/config/');
define('DIR_CACHE', 'C:/OpenServer/domains/upload/system/storage/cache/');
define('DIR_DOWNLOAD', 'C:/OpenServer/domains/upload/system/storage/download/');
define('DIR_LOGS', 'C:/OpenServer/domains/upload/system/storage/logs/');
define('DIR_MODIFICATION', 'C:/OpenServer/domains/upload/system/storage/modification/');
define('DIR_UPLOAD', 'C:/OpenServer/domains/upload/system/storage/upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'root');
define('DB_PASSWORD', '');
define('DB_DATABASE', 'opencarttest');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');
